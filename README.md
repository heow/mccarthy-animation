
## BUILD

`build-cljs-autobuild.sh`

`build-clj.sh`

## RUN

`start-app.sh` runs the Clojure application.

## DEBUG

`debug-app.sh`

In emacs fun `cider-connect` and point it to port 4006

## TODO

Collisions are calculated.

Only 1 game used background color to determine collisions, and it was a slow as when I tried it.

Collisions are easily calculated for round things, simple geometry.

For fast games, they use a model of the world and render based on it.  Almost all of the screen is broken up into blocks (like 20x20) and its easy to tell if your hero is touching one of the 20x20 blocks just 625 combinations on a 500px screen.

Perhaps use the best of both worlds, use the mccarthy-animation-mapper to blob up the screen using 20x20 blocks and not a 920x500 pixel array which is a data nightmare. 
